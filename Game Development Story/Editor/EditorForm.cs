﻿using GameDev.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Editor
{
    public partial class EditorForm : Form
    {
        public EditorForm()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                EventData eData = new EventData();

                eData.Name = Name_.Text;

                eData.Text = EventText.Text;
                //eData.MinDay = int.Parse(MinDay.Text);
                //eData.MaxDay = int.Parse(MaxDay.Text);
                eData.StunDuration = int.Parse(StunDuration.Text);
                eData.StunReason = StunReason.Text;
                /*eData.Probability = new int[6]
                    { int.Parse( Prob0.Text), int.Parse(Prob1.Text), int.Parse(Prob2.Text),
                    int.Parse(Prob3.Text), int.Parse(Prob4.Text), int.Parse(Prob5.Text) };
                    */
                eData.StressChange = int.Parse(StressChange.Text);
                eData.WillChange = int.Parse(WillChange.Text);
                eData.WorkForceChange = int.Parse(WorkForceChange.Text);
                eData.Consequence = Consequence.Text;
                eData.ConsequenceParameter = ConsequenceParameter.Text;
                /*
                eData.Resistances = new int[6]
                    {
                        int.Parse(textBox10.Text),int.Parse(textBox11.Text),int.Parse(textBox12.Text),
                        int.Parse(textBox13.Text),int.Parse(textBox14.Text),int.Parse(textBox15.Text)
                    };*/

                eData.AlwaysHappen = checkBox1.Checked;
                eData.Global = checkBox2.Checked;
                eData.ClubRoomOnly = checkBox3.Checked;
                eData.Repeatable = checkBox4.Checked;
                eData.HasChoices = checkBox5.Checked;

                eData.Choice1Text = Choice1Text.Text;
                eData.Choice1Paramter = Choice1Parameter.Text;
                eData.Choice1Consequence = Choice1Consequence.Text;
                eData.Choice1Response = Choice1Resp.Text;
                eData.Choice2Text = Choice2Text.Text;
                eData.Choice2Paramter = Choice2Parameter.Text;
                eData.Choice2Consequence = Choice2Consequence.Text;
                eData.Choice2Response = Choice2Resp.Text;

                eData.Xmlize(saveFileDialog.FileName);

            }
        }

        private void EditorForm_Load(object sender, EventArgs e)
        {

        }
    }
}

