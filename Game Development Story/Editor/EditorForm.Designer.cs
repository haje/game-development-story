﻿namespace Editor
{
    partial class EditorForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SaveButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.EventText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.MinDay = new System.Windows.Forms.TextBox();
            this.MaxDay = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Prob0 = new System.Windows.Forms.TextBox();
            this.Prob1 = new System.Windows.Forms.TextBox();
            this.Prob2 = new System.Windows.Forms.TextBox();
            this.Prob3 = new System.Windows.Forms.TextBox();
            this.Prob4 = new System.Windows.Forms.TextBox();
            this.Prob5 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.StunDuration = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.WorkForceChange = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.WillChange = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.StressChange = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Consequence = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Choice1Text = new System.Windows.Forms.TextBox();
            this.Choice1Consequence = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Choice2Consequence = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Choice2Text = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ConsequenceParameter = new System.Windows.Forms.TextBox();
            this.Choice1Parameter = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Choice2Parameter = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Choice1Resp = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Choice2Resp = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.Name_ = new System.Windows.Forms.TextBox();
            this.StunReason = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "xml 파일|*.xml";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(375, 255);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 0;
            this.SaveButton.Text = "저장";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "텍스트";
            // 
            // EventText
            // 
            this.EventText.Location = new System.Drawing.Point(12, 51);
            this.EventText.Multiline = true;
            this.EventText.Name = "EventText";
            this.EventText.Size = new System.Drawing.Size(133, 113);
            this.EventText.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(187, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "최소 발생 일자";
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(187, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "최대 발생 일자";
            this.label3.Visible = false;
            // 
            // MinDay
            // 
            this.MinDay.Location = new System.Drawing.Point(189, 51);
            this.MinDay.Name = "MinDay";
            this.MinDay.Size = new System.Drawing.Size(67, 21);
            this.MinDay.TabIndex = 5;
            this.MinDay.Text = "0";
            this.MinDay.Visible = false;
            // 
            // MaxDay
            // 
            this.MaxDay.Location = new System.Drawing.Point(189, 93);
            this.MaxDay.Name = "MaxDay";
            this.MaxDay.Size = new System.Drawing.Size(67, 21);
            this.MaxDay.TabIndex = 6;
            this.MaxDay.Text = "60";
            this.MaxDay.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(278, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "인원별 발생 확률";
            this.label4.Visible = false;
            // 
            // Prob0
            // 
            this.Prob0.Location = new System.Drawing.Point(280, 51);
            this.Prob0.Name = "Prob0";
            this.Prob0.Size = new System.Drawing.Size(67, 21);
            this.Prob0.TabIndex = 8;
            this.Prob0.Text = "0";
            this.Prob0.Visible = false;
            // 
            // Prob1
            // 
            this.Prob1.Location = new System.Drawing.Point(280, 78);
            this.Prob1.Name = "Prob1";
            this.Prob1.Size = new System.Drawing.Size(67, 21);
            this.Prob1.TabIndex = 9;
            this.Prob1.Text = "0";
            this.Prob1.Visible = false;
            // 
            // Prob2
            // 
            this.Prob2.Location = new System.Drawing.Point(280, 105);
            this.Prob2.Name = "Prob2";
            this.Prob2.Size = new System.Drawing.Size(67, 21);
            this.Prob2.TabIndex = 10;
            this.Prob2.Text = "0";
            this.Prob2.Visible = false;
            // 
            // Prob3
            // 
            this.Prob3.Location = new System.Drawing.Point(280, 132);
            this.Prob3.Name = "Prob3";
            this.Prob3.Size = new System.Drawing.Size(67, 21);
            this.Prob3.TabIndex = 11;
            this.Prob3.Text = "0";
            this.Prob3.Visible = false;
            // 
            // Prob4
            // 
            this.Prob4.Location = new System.Drawing.Point(280, 159);
            this.Prob4.Name = "Prob4";
            this.Prob4.Size = new System.Drawing.Size(67, 21);
            this.Prob4.TabIndex = 12;
            this.Prob4.Text = "0";
            this.Prob4.Visible = false;
            // 
            // Prob5
            // 
            this.Prob5.Location = new System.Drawing.Point(280, 186);
            this.Prob5.Name = "Prob5";
            this.Prob5.Size = new System.Drawing.Size(67, 21);
            this.Prob5.TabIndex = 13;
            this.Prob5.Text = "0";
            this.Prob5.Visible = false;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(383, 186);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(67, 21);
            this.textBox10.TabIndex = 20;
            this.textBox10.Text = "0";
            this.textBox10.Visible = false;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(383, 159);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(67, 21);
            this.textBox11.TabIndex = 19;
            this.textBox11.Text = "0";
            this.textBox11.Visible = false;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(383, 132);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(67, 21);
            this.textBox12.TabIndex = 18;
            this.textBox12.Text = "0";
            this.textBox12.Visible = false;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(383, 105);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(67, 21);
            this.textBox13.TabIndex = 17;
            this.textBox13.Text = "0";
            this.textBox13.Visible = false;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(383, 78);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(67, 21);
            this.textBox14.TabIndex = 16;
            this.textBox14.Text = "0";
            this.textBox14.Visible = false;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(383, 51);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(67, 21);
            this.textBox15.TabIndex = 15;
            this.textBox15.Text = "0";
            this.textBox15.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(381, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "인원별 저항 확률";
            this.label5.Visible = false;
            // 
            // StunDuration
            // 
            this.StunDuration.Location = new System.Drawing.Point(189, 132);
            this.StunDuration.Name = "StunDuration";
            this.StunDuration.Size = new System.Drawing.Size(67, 21);
            this.StunDuration.TabIndex = 22;
            this.StunDuration.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(187, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 12);
            this.label6.TabIndex = 21;
            this.label6.Text = "스턴 기간";
            // 
            // WorkForceChange
            // 
            this.WorkForceChange.Location = new System.Drawing.Point(96, 192);
            this.WorkForceChange.Name = "WorkForceChange";
            this.WorkForceChange.Size = new System.Drawing.Size(67, 21);
            this.WorkForceChange.TabIndex = 24;
            this.WorkForceChange.Text = "-60";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 12);
            this.label7.TabIndex = 23;
            this.label7.Text = "노동력 변화";
            // 
            // WillChange
            // 
            this.WillChange.Location = new System.Drawing.Point(96, 224);
            this.WillChange.Name = "WillChange";
            this.WillChange.Size = new System.Drawing.Size(67, 21);
            this.WillChange.TabIndex = 26;
            this.WillChange.Text = "-60";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 227);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 12);
            this.label8.TabIndex = 25;
            this.label8.Text = "의욕 변화";
            // 
            // StressChange
            // 
            this.StressChange.Location = new System.Drawing.Point(96, 257);
            this.StressChange.Name = "StressChange";
            this.StressChange.Size = new System.Drawing.Size(67, 21);
            this.StressChange.TabIndex = 28;
            this.StressChange.Text = "-60";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 260);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 12);
            this.label9.TabIndex = 27;
            this.label9.Text = "스트레스 변화";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(280, 213);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(88, 16);
            this.checkBox1.TabIndex = 29;
            this.checkBox1.Text = "반드시 발생";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(280, 235);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(48, 16);
            this.checkBox2.TabIndex = 30;
            this.checkBox2.Text = "전역";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(280, 257);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(76, 16);
            this.checkBox3.TabIndex = 31;
            this.checkBox3.Text = "동방 전용";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(371, 213);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(76, 16);
            this.checkBox4.TabIndex = 32;
            this.checkBox4.Text = "반복 가능";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(371, 235);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(88, 16);
            this.checkBox5.TabIndex = 33;
            this.checkBox5.Text = "선택지 존재";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(187, 204);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 12);
            this.label10.TabIndex = 34;
            this.label10.Text = "결과 함수";
            // 
            // Consequence
            // 
            this.Consequence.Location = new System.Drawing.Point(189, 218);
            this.Consequence.Name = "Consequence";
            this.Consequence.Size = new System.Drawing.Size(83, 21);
            this.Consequence.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(485, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 12);
            this.label11.TabIndex = 36;
            this.label11.Text = "선택지1 텍스트";
            // 
            // Choice1Text
            // 
            this.Choice1Text.Location = new System.Drawing.Point(487, 51);
            this.Choice1Text.Name = "Choice1Text";
            this.Choice1Text.Size = new System.Drawing.Size(101, 21);
            this.Choice1Text.TabIndex = 37;
            // 
            // Choice1Consequence
            // 
            this.Choice1Consequence.Location = new System.Drawing.Point(487, 96);
            this.Choice1Consequence.Name = "Choice1Consequence";
            this.Choice1Consequence.Size = new System.Drawing.Size(101, 21);
            this.Choice1Consequence.TabIndex = 39;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(485, 81);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 12);
            this.label12.TabIndex = 38;
            this.label12.Text = "선택지1 결과 함수";
            // 
            // Choice2Consequence
            // 
            this.Choice2Consequence.Location = new System.Drawing.Point(487, 222);
            this.Choice2Consequence.Name = "Choice2Consequence";
            this.Choice2Consequence.Size = new System.Drawing.Size(101, 21);
            this.Choice2Consequence.TabIndex = 43;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(485, 207);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 12);
            this.label13.TabIndex = 42;
            this.label13.Text = "선택지2 결과 함수";
            // 
            // Choice2Text
            // 
            this.Choice2Text.Location = new System.Drawing.Point(487, 177);
            this.Choice2Text.Name = "Choice2Text";
            this.Choice2Text.Size = new System.Drawing.Size(101, 21);
            this.Choice2Text.TabIndex = 41;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(485, 162);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 12);
            this.label14.TabIndex = 40;
            this.label14.Text = "선택지2 텍스트";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(187, 242);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 12);
            this.label15.TabIndex = 44;
            this.label15.Text = "함수 파라미터";
            // 
            // ConsequenceParameter
            // 
            this.ConsequenceParameter.Location = new System.Drawing.Point(189, 257);
            this.ConsequenceParameter.Name = "ConsequenceParameter";
            this.ConsequenceParameter.Size = new System.Drawing.Size(83, 21);
            this.ConsequenceParameter.TabIndex = 45;
            // 
            // Choice1Parameter
            // 
            this.Choice1Parameter.Location = new System.Drawing.Point(487, 135);
            this.Choice1Parameter.Name = "Choice1Parameter";
            this.Choice1Parameter.Size = new System.Drawing.Size(83, 21);
            this.Choice1Parameter.TabIndex = 47;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(485, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 12);
            this.label16.TabIndex = 46;
            this.label16.Text = "함수 파라미터";
            // 
            // Choice2Parameter
            // 
            this.Choice2Parameter.Location = new System.Drawing.Point(487, 261);
            this.Choice2Parameter.Name = "Choice2Parameter";
            this.Choice2Parameter.Size = new System.Drawing.Size(83, 21);
            this.Choice2Parameter.TabIndex = 49;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(485, 246);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 12);
            this.label17.TabIndex = 48;
            this.label17.Text = "함수 파라미터";
            // 
            // Choice1Resp
            // 
            this.Choice1Resp.Location = new System.Drawing.Point(594, 51);
            this.Choice1Resp.Name = "Choice1Resp";
            this.Choice1Resp.Size = new System.Drawing.Size(101, 21);
            this.Choice1Resp.TabIndex = 51;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(592, 36);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 12);
            this.label18.TabIndex = 50;
            this.label18.Text = "선택지에 따른 반응";
            // 
            // Choice2Resp
            // 
            this.Choice2Resp.Location = new System.Drawing.Point(594, 177);
            this.Choice2Resp.Name = "Choice2Resp";
            this.Choice2Resp.Size = new System.Drawing.Size(101, 21);
            this.Choice2Resp.TabIndex = 53;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(592, 162);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(109, 12);
            this.label19.TabIndex = 52;
            this.label19.Text = "선택지에 따른 반응";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 12);
            this.label20.TabIndex = 54;
            this.label20.Text = "이름";
            // 
            // Name_
            // 
            this.Name_.Location = new System.Drawing.Point(47, 6);
            this.Name_.Name = "Name_";
            this.Name_.Size = new System.Drawing.Size(485, 21);
            this.Name_.TabIndex = 55;
            // 
            // StunReason
            // 
            this.StunReason.Location = new System.Drawing.Point(189, 174);
            this.StunReason.Name = "StunReason";
            this.StunReason.Size = new System.Drawing.Size(67, 21);
            this.StunReason.TabIndex = 57;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(187, 159);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 12);
            this.label21.TabIndex = 56;
            this.label21.Text = "스턴 사유";
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 284);
            this.Controls.Add(this.StunReason);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Name_);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Choice2Resp);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Choice1Resp);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Choice2Parameter);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Choice1Parameter);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.ConsequenceParameter);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Choice2Consequence);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Choice2Text);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Choice1Consequence);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Choice1Text);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Consequence);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.StressChange);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.WillChange);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.WorkForceChange);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.StunDuration);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Prob5);
            this.Controls.Add(this.Prob4);
            this.Controls.Add(this.Prob3);
            this.Controls.Add(this.Prob2);
            this.Controls.Add(this.Prob1);
            this.Controls.Add(this.Prob0);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.MaxDay);
            this.Controls.Add(this.MinDay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.EventText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SaveButton);
            this.Name = "EditorForm";
            this.Text = "Editor";
            this.Load += new System.EventHandler(this.EditorForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox EventText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox MinDay;
        private System.Windows.Forms.TextBox MaxDay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Prob0;
        private System.Windows.Forms.TextBox Prob1;
        private System.Windows.Forms.TextBox Prob2;
        private System.Windows.Forms.TextBox Prob3;
        private System.Windows.Forms.TextBox Prob4;
        private System.Windows.Forms.TextBox Prob5;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox StunDuration;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox WorkForceChange;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox WillChange;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox StressChange;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Consequence;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Choice1Text;
        private System.Windows.Forms.TextBox Choice1Consequence;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Choice2Consequence;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Choice2Text;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox ConsequenceParameter;
        private System.Windows.Forms.TextBox Choice1Parameter;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Choice2Parameter;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Choice1Resp;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Choice2Resp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Name_;
        private System.Windows.Forms.TextBox StunReason;
        private System.Windows.Forms.Label label21;
    }
}

