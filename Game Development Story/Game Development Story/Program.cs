﻿using System;
using System.Collections.Generic;
using GameDev.UI;
using GameDev.Data;
using System.Windows.Forms;

using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Media;

namespace GameDev
{
    class Program
    {
        static Random random = new Random();

        const int MaxMessageLineNumber = 17;

        // 이 클래스가 전체 게임과 관련된 모든 일을 다 할 겁니다 아주 비대한 클래스가 될 거예요
        // 콘솔 관련은 죄다 이 클래스에 몰아넣고 싶은 욕구가 있는데 어떻게 할지 잘 모르겠습니다.

        public static void PlayMusic()
        {
            SoundPlayer player = new SoundPlayer();
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "/Music/Chiptune #1.wav";
            player.PlayLooping();
        }

        [STAThread]
        static void Main(string[] args)
        {
            InitializePool();
            PlayMusic();
            initializeEndingPool();
            // EventPool에 자동으로 물건들이 추가될 겁니다.

            Intro();

            //인간 생성
            InitParty();

            while (!MustQuit)
            {
                DisplayScreen();
                AskForChoice();
            }
        }

        static bool OkNextDay = true;
        static bool MustQuit = false;

        static void AskForChoice()
        {
            if (EventMethods.AffectedMember != null)
            {
                StatusMessage = (EventMethods.AffectedMember.Name + " : " + "나를 어떻게 할 셈이지?\n");
                DisplayScreen();
            }

            while (!MustQuit)
            {
                char key = Console.ReadKey(true).KeyChar;

                if (key == 'q')
                {
                    MustQuit = true;
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                    return;
                }
                else if (key == 'c')
                {
                    ShowCredits();

                    DisplayScreen();
                    continue;
                }

                else if (key == 'n' && OkNextDay)
                {
                    NewDay();
                    continue;
                }

                foreach (Choice choice in currentEvent.Choices)
                {
                    if (key == choice.Key)
                    {
                        AddMessage("{white}'{/white}{darkcyan}" + choice.Text + "{/darkcyan}{white}'을 선택했습니다.{/white}");
                        choice.Consequence();
                        return;
                    }
                }

                if (key == 'r' && isEnding)
                {
                    CurrentDateTime = new DateTime(2016, 7, 1);

                    CurrentProject = new Project();
                    currentEvent = new GameEvent();

                    Messages = new List<string>() { "", "", "", "", "", "", "" };

                    StatusMessage = "\n\n";
                    NewsMessages = "\n";

                    isEnding = false;

                    Display.Clear();
                    Intro();
                    InitParty();
                }
                if (Control.IsKeyLocked(Keys.CapsLock) == false)
                    AddMessage("{yellow}오류: 존재하지 않는 명령어입니다.{/yellow}");
                else
                    AddMessage("{yellow}오류: 존재하지 않는 명령어입니다. Caps Lock이 켜져 있습니다.{/yellow}");
            }
        }

        static void CreateUnits()
        {

        }

        /// <summary>
        /// 새로운 날이 밝습니다. 봙봙
        /// </summary>
        static void NewDay()
        {
            foreach (EndingEvent e in EndingEventPool)
            {
                if (e.checkFunc(Members, CurrentProject))
                {
                    e.EventTrigger();
                    return;
                }
            }
            OkNextDay = false;
            if (CurrentDateTime == new DateTime(2016, 9, 1))
            {
                //Game Over
            }
            //캐릭터 스탯 증감

            //캐릭터가 회복을 합니다
            foreach (Member m in Members)
            {
                m.Will = Math.Min(m.Will + (int)(m.Heal * (100 - m.StressLevel) / 100.0f), 100);
                m.StressLevel = Math.Max(0, m.StressLevel - (int)(m.Heal * (100 - m.StressLevel)/100.0f));
            }

            if (false)//필수 이벤트 존재
            {
                //필수 이벤트 진행
            }
            else
            {
                // 출석주사위
                bool butNoOneCame = true;
                foreach (Member m in Members)
                {
                    if (m == zzt) continue;
                    if (m.Attendence = RandomHelper.RollDice() <= m.AttendenceProbability) butNoOneCame = false;
                }

                if (butNoOneCame)
                {
                    Members[0].StressLevel += 30;
                }
                else
                {
                    //NewsMessages = EventPool[RandomHelper.RandRange(0, EventPool.Count - 1)].Data.Name;
                    foreach (Member m in Members)
                    {
                        if (m == zzt) continue;
                        if (!m.Attendence) continue;
                        if (m.RemainingStunTurn != 0)
                        {

                            AddMessage(String.Format(@"{0}은/는 {1} 사유로 {2}턴간 스턴", m.Name, m.StunReason, m.RemainingStunTurn));
                        }
                        else
                        {
                            // random event
                            GameEvent e = EventPool[RandomHelper.RandRange(0, EventPool.Count - 1)];

                            DoEvent(m, e);
                            Display.DrawEventSeparator();
                        }
                    }
                }

                foreach (Member m in Members)
                {
                    // 게임 개발 기여 (진행도 상승)
                    // c - ix.
                    CurrentProject.Completion += m.GetWork();
                    m.RemainingStunTurn = Math.Max(0, m.RemainingStunTurn - 1);
                }

            }
            CurrentDateTime = CurrentDateTime.AddDays(1);
            OkNextDay = true;
            DisplayScreen();
        }

        public static void DoEvent(Member m, GameEvent e)
        {
            AddMessage("☆" + e.Data.Name + "☆");
            AddMessage(e.Data.Text);

            //이벤트를 진행
            EventMethods.AffectedMember = m;

            if (e.Data.WillChange != 0)
            {
                AddMessage(m.Name + "의 의욕이 " + e.Data.WillChange + "만큼 변경되었습니다.");
                m.Will += e.Data.WillChange;
            }

            if (e.Data.StressChange != 0)
            {
                AddMessage(m.Name + "의 스트레스가 " + e.Data.StressChange + "만큼 변경되었습니다.");
                m.StressLevel += e.Data.StressChange;
            }


            if (e.Data.WorkForceChange != 0)
            {
                AddMessage(m.Name + "의 노동력이 " + e.Data.WorkForceChange + "만큼 변경되었습니다.");
                m.StressLevel += e.Data.WorkForceChange;
            }

            if (e.Data.StunDuration != 0)
            {
                AddMessage(m.Name + "이(가) " + e.Data.StunReason + " 때문에 " + e.Data.StunDuration + "일만큼 스턴되어 있게 됩니다.");
                m.GetStun(e.Data.StunDuration, e.Data.StunReason);
            }

            m.Recalculate();

            currentEvent = e;
            DisplayScreen();
            AskForChoice();

            EventMethods.AffectedMember = null;
            currentEvent = new GameEvent();

        }

        public static DateTime CurrentDateTime = new DateTime(2016, 7, 1);

        public static Project CurrentProject = new Project();
        public static GameEvent currentEvent = new GameEvent();

        public static List<String> Messages = new List<string>(new string[MaxMessageLineNumber]);

        public static string StatusMessage = "\n\n";
        public static string NewsMessages = "\n";

        public static bool isEnding = false;

        //public static Member zzt = new Member();
        public static Member zzt;

        //public static List<Member> Members = new List<Member>() { zzt };
        public static List<Member> Members = new List<Member>();

        public static List<GameEvent> EventPool = new List<GameEvent>();
        public static List<EndingEvent> EndingEventPool = new List<EndingEvent>();

        public static void InitializePool()
        {
            EventPool = new List<GameEvent>();
            string path = AppDomain.CurrentDomain.BaseDirectory;

            foreach (string file in Directory.GetFiles(path + "Events\\", "*.xml"))
            {
                EventData eData = EventData.XmlLoad(file);
                GameEvent ev = new GameEvent(eData);
                EventPool.Add(ev);
            }
        }

        public static void initializeEndingPool()
        {
            EndingEventPool = new List<EndingEvent>();
            EndingEvent happy_ending = new EndingEvent();
            happy_ending.Name = "★Happy Ending★";
            happy_ending.checkFunc =
                (l, p) => p.Completion >= 10000;
            happy_ending.Text.Add("구라치면");
            happy_ending.Text.Add("손모가지 날아가요");
            EndingEventPool.Add(happy_ending);

            EndingEvent zzt_ending = new EndingEvent();
            zzt_ending.Name = "★Bad Ending★";
            zzt_ending.checkFunc = checkZZT;
            zzt_ending.Text.Add("으으으");
            zzt_ending.Text.Add("으으으으으으");
            zzt_ending.Text.Add("도저히 참을 수 없다!");
            zzt_ending.Text.Add("도저히 참을 수 없어!!!!");
            zzt_ending.Text.Add("\n");
            zzt_ending.Text.Add("\n");
            zzt_ending.Text.Add("회장은 결국");
            zzt_ending.Text.Add("'인성과 권력'을 사용했다.");
            zzt_ending.Text.Add("\n");
            zzt_ending.Text.Add("\n");
            zzt_ending.Text.Add("   모든 권력은 회장에 있으며");
            zzt_ending.Text.Add("   회장 외에는 아무것도 없으며");
            zzt_ending.Text.Add("   회장에 반항하는 자도 존재하지 않는다");
            zzt_ending.Text.Add("\n");
            zzt_ending.Text.Add("       ~END~");
            EndingEventPool.Add(zzt_ending);

            EndingEvent Nexon_Ending = new EndingEvent();
            Nexon_Ending.Name = "★대망의 넥슨 발표날★";
            Nexon_Ending.Text.Add("토막 상식 : 넥슨 발표는 고기 먹는 날이다.");
            Nexon_Ending.Text.Add("우리 동아리는 안타깝게도");
            Nexon_Ending.Text.Add("고기를 조금 덜 먹었다");
            Nexon_Ending.Text.Add("하지만 올해도 다행히 발표를 하고 살아남았다.");
            Nexon_Ending.Text.Add("Normal Ending");
            Nexon_Ending.checkFunc = checkNexon_Ending;
            EndingEventPool.Add(Nexon_Ending);

            EndingEvent RACRISH = new EndingEvent();
            RACRISH.Name = "★BADENDING★";
            RACRISH.Text.Add("리빙 포인트 : 화나면 라크쉬르를 신청한다");
            RACRISH.Text.Add("\n");
            RACRISH.Text.Add("회원 : 도저히 참을 수 없소 회장.");
            RACRISH.Text.Add("회원 : 우리는 당신의 폭정에 도저히 참을 수 없어");
            RACRISH.Text.Add("회원 : 회장에게 라크쉬르를 신청하겠소!");
            RACRISH.Text.Add("\n");
            RACRISH.Text.Add("\n");
            RACRISH.Text.Add("뭔 개소리여?");
            RACRISH.Text.Add("\n");
            RACRISH.Text.Add("회원들 : 무지개 빛 총 공격이다!");
            RACRISH.Text.Add("콰아아아");
            RACRISH.Text.Add("------ 말 도 아아안 돼애애애애 ------");
            RACRISH.Text.Add("\n");
            RACRISH.checkFunc = checkRACRISH;
            EndingEventPool.Add(RACRISH);

            EndingEvent TYRAEL = new EndingEvent();
            TYRAEL.Name = "★STRESS_MAX★";
            TYRAEL.Text.Add("리빙 포인트 : 팀원을 굴리지 맙시다");
            TYRAEL.Text.Add("\n");
            TYRAEL.Text.Add("회장: 드높은 하제의 고대 법률은");
            TYRAEL.Text.Add("회장: 개발실에서 게임하는 것을 엄격히 금한다!");
            TYRAEL.Text.Add("회장: 그런 짓을 하다니 뻔뻔하구나!");
            TYRAEL.Text.Add("\n");
            TYRAEL.Text.Add("\n");
            TYRAEL.Text.Add("회원: 내 죄라면 갓-겜을 했다는 것뿐이다. 회장.");
            TYRAEL.Text.Add("회원: 넌 권력 뒤에 숨었지만 말이다!");
            TYRAEL.Text.Add("\n");
            TYRAEL.Text.Add("회장: 닥쳐라!");
            TYRAEL.Text.Add("회장: 네 죗값을 당장 치르게 해주마!");
            TYRAEL.Text.Add("\n");
            TYRAEL.Text.Add("회원: 누가 나를 구속하는가!");
            TYRAEL.Text.Add("회원: 내가 바로 정의다!");
            TYRAEL.Text.Add("회원: 우리에겐 더 큰 숙명이 있다.");
            TYRAEL.Text.Add("회원: 갓갓게임을 만드는 것이지.");
            TYRAEL.Text.Add("회원: 그러나 그 잘난 법이 그대 모두를 얽맨다면");
            TYRAEL.Text.Add("회원: 이제 하제에 남지 않겠다");
            TYRAEL.Text.Add("\n");
            zzt_ending.Text.Add("       ~END~");
            TYRAEL.checkFunc = checkTYRAEL;
            EndingEventPool.Add(TYRAEL);
        }

        public static bool checkNexon_Ending(List<Member> m, Project p)
        {
            DateTime asdf = new DateTime(2016, 8, 31);
            return (CurrentDateTime.Date == asdf.Date);
        }

        public static bool checkTYRAEL(List<Member> m, Project p)
        {

            foreach (Member mmm in m)
            {
                if (mmm.StressLevel >= 95) return true;
            }

            return false;
        }

        public static bool checkRACRISH(List<Member> m, Project p)
        {
            int asdf = 0;

            foreach (Member mmm in m)
            {
                asdf += mmm.StressLevel;
            }

            return (asdf >= 375);
        }
        public static bool checkZZT(List<Member> m, Project p)
        {
            return m[0].StressLevel >= 100;
            //return true;
        }

        public static void AddMessage(string msg, bool refreshScreen = true)
        {
            foreach (string line in msg.Split('\n'))
            {
                Messages.Add(line);
            }
            while (Messages.Count > MaxMessageLineNumber)
            {
                Messages.RemoveAt(0);
            }

            if (refreshScreen)
            {
                if (isEnding) DisplayEndingScreen();
                else DisplayScreen();
            }
        }

        #region Auxiliary
        /// <summary>
        /// 인트로를 출력합니다. 지금 있는 글귀는 임시입니다.
        /// </summary>
        static void Intro()
        {
            bool flag = false;
        Intro:
            Console.Clear();

            Display.WriteColor("{white}당신은 회장입니다.\n이 한심하기 짝이 없는 동아리원들이 게임을 좀 만들게 하십시오.\n\n그렇다면 당신의 이름은?\n{/white}");

            if (flag)
                Display.WriteColor("{white}전각은 3글자, 반각은 7글자 까지만 가능합니다.\n\n{/white}");

            flag = false;
            //Console.ReadKey(true);

            {
                string namename = Console.ReadLine();
                char[] charArr = namename.ToCharArray();
                foreach (char c in charArr)
                {
                    if (char.GetUnicodeCategory(c) == System.Globalization.UnicodeCategory.OtherLetter)
                    {
                        flag = true;
                        break;
                    }
                }
                if ((flag && namename.Length > 3) || (namename.Length > 7))
                {
                    flag = true;
                    goto Intro;
                }

                char key;

                flag = false;

                if (namename.ToLower() == "zzt" || namename == "원정호")
                {
                    Display.WriteColor("\n{white}이 이름을 선택할 시{/white} {red}HARD MODE{/red} {white}로 진행됩니다.\n계속 진행하시겠습니까?\nY / N\n{/white}");

                    flag = true;

                }
                else if (namename.ToLower() == "ryang" || namename == "정기량")
                {
                    Display.WriteColor("\n{white}좀 더 좋은 이름 없냐?{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "nogoon" || namename == "노정훈")
                {
                    Display.WriteColor("\n{white}그런 사람이 있었나...?{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "cehigi" || namename == "김준")
                {
                    Display.WriteColor("\n{white}난 언제나 최고의 맛과 정성을 다한다!{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "jinyong" || namename == "박진용")
                {
                    Display.WriteColor("\n{white}난 이미 1년했다.{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "jacka" || namename == "이승민")
                {
                    Display.WriteColor("\n{white}어떻게 사람이 회장을 할 수 있죠?{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "fox")
                {
                    Display.WriteColor("\n{white}여우! 여우! 여우!{/white}\n\n");
                }
                else if (namename.ToLower() == "zenta" || namename == "오진석")
                {
                    Display.WriteColor("\n{white}저리가 이 악마야!{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "namkyu" || namename.ToLower() == "강남규")
                {
                    Display.WriteColor("\n{white}맞을래?{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "kev" || namename.ToLower() == "백교풍")
                {
                    Display.WriteColor("\n{white}  연  개  소  문  {/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "aru" || namename.ToLower() == "이재영")
                {
                    Display.WriteColor("\n{white}시이이이이이이이러어어어어어어어{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename.ToLower() == "atonal" || namename.ToLower() == "이준희")
                {
                    Display.WriteColor("\n{white}그 분은 물을 마시고 계십니다.{/white}\n\n");

                    Console.ReadKey(true);
                    goto Intro;
                }
                else if (namename == "원설호")
                {
                    Display.WriteColor("\n{white}뭐... 이정도면...{/white}\n\n");
                }

                if (flag)
                {
                    while (true)
                    {
                        key = Console.ReadKey(true).KeyChar;

                        if (key == 'n' || key == 'N')
                        {
                            flag = false;
                            goto Intro;
                        }
                        else if (key == 'y' || key == 'Y')
                        {
                            break;
                        }
                    }
                }
                else
                {
                    Display.WriteColor("\n{white}이 이름으로 하시겠습니까?\nY / N{/white}\n\n");

                    while (true)
                    {
                        key = Console.ReadKey(true).KeyChar;

                        if (key == 'n' || key == 'N')
                        {
                            flag = false;
                            goto Intro;
                        }
                        else if (key == 'y' || key == 'Y')
                        {
                            break;
                        }
                    }
                }

                zzt = new Member(namename);

            }
        }

        /// <summary>
        /// 크레딧을 표시합니다.
        /// </summary>
        static void ShowCredits()
        {
            Display.Clear();

            Display.WriteColor("{yellow}Game Development Story\n\n-*-*-*-*-*-*-*-*-*-*-*-*-*-\n{/yellow}" +
                "{darkcyan}Aru\nCehigi\nJacka\nKeV\n박진용\n강남규\nNogoon\nRyang\nZentroll\nzzt\n{/darkcyan}" +
                "{yellow}-*-*-*-*-*-*-*-*-*-*-*-*-*-{/yellow}\n\n" +
                "{darkgreen}Special Thanks To:{/darkgreen}\n{cyan}Atonal{/cyan}\n\n{white}아무 키나 누르면 계속됩니다.{/white}");
            Console.ReadKey(true);
        }
        #endregion


        /// <summary>
        /// 기본 게임 화면을 표시합니다.
        /// </summary>
        static void DisplayScreen()
        {
            Display.Clear();

            DisplayDateCompletion();
            Display.DrawSeparator();

            DisplayNews();
            Display.DrawSeparator();

            DisplayMemberStats();
            Display.DrawSeparator();

            DisplayStatusMessage();
            Display.DrawSeparator();

            DisplayMessages();
            Display.DrawSeparator();

            DisplayChoices();
            Display.DrawSeparator();
        }

        /// <summary>
        /// 엔딩 게임 화면을 표시합니다.
        /// </summary>
        public static void DisplayEndingScreen()
        {
            Display.Clear();

            StatusMessage = "\n\n";

            DisplayDateCompletion();
            Display.DrawSeparator();

            DisplayNews();
            Display.DrawSeparator();

            DisplayMemberStats();
            Display.DrawSeparator();

            DisplayStatusMessage();
            Display.DrawSeparator();

            DisplayMessages();
            Display.DrawSeparator();

            DisplayEndingChoices();
            Display.DrawSeparator();
        }


        /// <summary>
        /// 날짜와 현재 완성도를 표시합니다.
        /// </summary>
        static void DisplayDateCompletion()
        {
            Display.WriteColor(CurrentDateTime.ToString("{white}yyyy년 MM월 dd일{/white}")
                + "\t\t{white}현재 게임 퀄리티: " + (int)CurrentProject.Completion + " 완성력{/white}\n");
        }

        /// <summary>
        /// 최근의 주요 사건을 표시합니다.
        /// </summary>
        static void DisplayNews(string msg = "")
        {
            Display.WriteColor("{white}" + NewsMessages + "{/white}");
        }

        static void DisplayStatusMessage()
        {
            Display.WriteColor("{white}" + StatusMessage + "{/white}");
        }

        /// <summary>
        /// 멤버들의 상태를 표시합니다.
        /// </summary>
        static void DisplayMemberStats()
        {
            Display.WriteColor("{white}이름　\t스트레스\t노동력\t의욕\t상태\n{/white}");

            foreach (Member memeber in Members)
            {
                string nameColor = memeber.Attendence ? "darkcyan" : "gray";
                Display.WriteColor(String.Format(
                    "{{{5}}}{0}{{/{5}}}\t{{white}}{1}\t\t{2}\t{3}\t{4}{{/white}}\n",
memeber.Name, memeber.StressLevel, memeber.WorkForce, memeber.Will, memeber.Attendence ?
(memeber.RemainingStunTurn > 0 ? memeber.StunReason : "") : "{gray}결근{/gray}", nameColor));
            }
        }

        /// <summary>
        /// 대사를 표시합니다.
        /// </summary>
        static void DisplayMessages()
        {
            foreach (string line in Messages)
                Display.WriteColor(line + "\n");
        }

        /// <summary>
        /// 선택지들을 출력합니다.
        /// </summary>
        static void DisplayChoices()
        {

            //Display.WriteColor(currentChoiceEvent.Text + "\n");

            foreach (Choice choice in currentEvent.Choices)
            {
                Display.WriteColor("{yellow}" + choice.Key.ToString() + "{/yellow}:\t{white} " + choice.Text + "{/white}\n");
            }

            Display.WriteColor
                (
                "{yellow}q{/yellow}{white}:\t게임 종료\n{/white}"
            + "{yellow}c{/yellow}{white}:\t크레딧 보기\n{/white}"
            + (OkNextDay ? "{yellow}n{/yellow}{white}:\t다음날\n{/white}" : "")
            );


        }
        public static void DisplayEndingChoices()
        {
            Display.WriteColor("{yellow}q{/yellow}{white}:\t게임 종료\n{/white}"
                + "{yellow}c{/yellow}{white}:\t크레딧 보기\n{/white}"
                + "{yellow}r{/yellow}{white}:\t다시 플레이하기\n{/white}");
        }

        public static void InitParty()
        {
            Members = new List<Member>();
            Members.Add(zzt);
            zzt.Attendence = true;
            Random r = new Random();
            List<string> namelist = new List<string>
            {
                "애국러", "토랑스", "C리건", "펄스나", "한종",
                "오텟", "이카림", "오중주" , "간좀", "고대신",
                "탭스타", "고양이", "대군주", "인권", "슬라임",
                "크리퍼", "슈", "홍도타","아존게"
            };
            for (int i = 0; i < 5; i++)
            {
                int rr = r.Next(0, namelist.Count);
                Members.Add(new Member(namelist[rr]));
                namelist.RemoveAt(rr);
            }
        }


        #region Unit Test

        static void Troll()
        {
            System.Diagnostics.Process.Start("http://kr.battle.net/heroes/ko/");
        }
        #endregion
    }

}
