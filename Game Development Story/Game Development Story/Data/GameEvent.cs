﻿using System.Collections.Generic;
using System;
using System.Reflection;

namespace GameDev.Data
{
    public class GameEvent
    {
        public List<Choice> Choices = new List<Choice>();
        public Action Consequence;
        public EventData Data;

        public GameEvent() { Data = new EventData(); }

        public GameEvent(EventData data)
        {
            Data = data;
            if (data.Consequence != "")
                Consequence = () =>
                {
                    typeof(EventMethods).GetMethod(data.Consequence).Invoke(null, new object[] { data.ConsequenceParameter });
                };

            if (true/*data.HasChoices*/)
            {
                Choice choice1 = new Choice();
                choice1.Text = data.Choice1Text;
                choice1.Key = 'a';
                if (data.Choice1Consequence != "")
                    choice1.Consequence = () =>
                    {
                        Program.AddMessage("{white}" + data.Choice1Response + "{/white}\n");
                        typeof(EventMethods).GetMethod(data.Choice1Consequence).Invoke(null, new object[] { data.Choice1Paramter });
                    };
                else
                    choice1.Consequence = () =>
                    {
                        Program.AddMessage("{white}" + data.Choice1Response + "{/white}\n");
                        //Program.AddMessage("{white}능력치에 변화가 없습니다.{/white}");
                    };

                Choice choice2 = new Choice();
                choice2.Text = data.Choice2Text;
                choice2.Key = 's';
                if (data.Choice2Consequence != "")
                    choice2.Consequence = () =>
                    {
                        Program.AddMessage("{white}" + data.Choice2Response + "{/white}\n");
                        typeof(EventMethods).GetMethod(data.Choice2Consequence).Invoke(null, new object[] { data.Choice2Paramter });
                    };
                else
                    choice2.Consequence = () =>
                    {
                        Program.AddMessage("{white}" + data.Choice2Response + "{/white}\n");
                        //Program.AddMessage("{white}능력치에 변화가 없습니다.{/white}");
                    };

                Choices.Add(choice1);
                Choices.Add(choice2);
            }
        }
    }
}
