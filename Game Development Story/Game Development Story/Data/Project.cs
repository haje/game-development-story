﻿namespace GameDev.Data
{
    public enum QualityLevel
    {
        F = 0,
        D = 1,
        C = 2,
        B = 3,
        A = 4,
        S = 5,
    }

    public class Project
    {
        public QualityLevel Quality;

        public float Progress;

        public void ModifyQuality(int level)
        {
            int qual = (int)Quality + level;
            if (qual < 0) qual = 0;
            if (qual > 5) qual = 5;

            Quality = (QualityLevel)qual;
        }

        public void UpdateProgress(float works)
        {
            Progress += works;
            Quality = (QualityLevel)((int)Progress);
        }

        public float Completion;

    }
}
 