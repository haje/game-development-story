﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace GameDev.Data
{
    [Serializable]
    public class EventData
    {
        public string Name;

        public string Text;
        /*
        public int MinDay;
        public int MaxDay;
        */
        //public int[] Probability = new int[6] { 0, 0, 0, 0, 0, 0 };

        public string Consequence;
        public string ConsequenceParameter;

        //public int[] Resistances = new int[6] { 0, 0, 0, 0, 0, 0 };
        public string StunReason;
        public int StunDuration;

        public int WorkForceChange;
        public int StressChange;
        public int WillChange;

        public bool AlwaysHappen;

        public bool Global;
        public bool ClubRoomOnly;

        public bool Repeatable;
        public bool HasChoices;

        public string Choice1Text;
        public string Choice1Consequence;
        public string Choice1Paramter;
        public string Choice1Response;

        public string Choice2Text;
        public string Choice2Consequence;
        public string Choice2Paramter;
        public string Choice2Response;

        #region XML
        static public void Xmlize(EventData eData, string path)
        {
            XmlSerializer ser = new XmlSerializer(typeof(EventData));
            XmlWriterSettings ws = new XmlWriterSettings();
            ws.NewLineHandling = NewLineHandling.Entitize;
            using (var writer = XmlWriter.Create(path, ws))
            {
                ser.Serialize(writer, eData);
            }
        }
        static public void Xmlize(EventData eData)
        {
            Xmlize(eData, @"C:\Users\하제\Desktop\새 폴더\Test.xml");
        }
        public void Xmlize(string path)
        {
            Xmlize(this, path);
        }
        public void Xmlize()
        {
            Xmlize(this);
        }
        static public EventData XmlLoad(string path)
        {
            EventData eData;
            using (TextReader reader = new StreamReader(path))
            {
                XmlSerializer ser = new XmlSerializer(typeof(EventData));
                eData = (EventData)ser.Deserialize(reader);
            }
            return eData;
        }
        #endregion
    }
}
