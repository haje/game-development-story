﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GameDev.Data
{
    public static class EventMethods
    {
        public static Member AffectedMember = null;

        public static void StunMember(string TurnAndReason)
        {
            string[] tar = TurnAndReason.Split(',');
            Debug.Assert(tar.Length == 2);

            int turn = int.Parse(tar[0]);
            string reason = tar[1];

            Program.AddMessage(AffectedMember.Name + "이 " + reason + " 때문에 " + turn + "일만큼 스턴되어 있게 됩니다.");
            AffectedMember.GetStun(turn, reason);

        }

        public static void ModifyWill(string value)
        {
            Program.AddMessage(AffectedMember.Name + "의 의욕이 " + value + "만큼 변경되었습니다.");
            AffectedMember.Will += int.Parse(value);
            AffectedMember.Recalculate();
        }

        public static void ModifyWorkForce(string value)
        {
            Program.AddMessage(AffectedMember.Name + "의 노동력이 "  + value + "만큼 변경되었습니다.");
            AffectedMember.WorkForce += int.Parse(value);
            AffectedMember.Recalculate();
        }

        public static void ModifyStress(string value)
        {
            Program.AddMessage(AffectedMember.Name + "의 스트레스가 " + value + "만큼 변경되었습니다.");
            AffectedMember.StressLevel += int.Parse(value);
            AffectedMember.Recalculate();
        }
    }
}
