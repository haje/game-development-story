﻿using System;

namespace GameDev.Data
{
    public class Choice
    {
        public string Text;

        public char Key = 'a';
        //public string 

        public Action Consequence = DoNothing;

        static void DoNothing()
        {
            Program.AddMessage("{white}아무 일도 일어나지 않았습니다.{/white}");
        }
    }
}
