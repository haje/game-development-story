﻿using System;
using System.Collections.Generic;

using System.Xml;
using System.Xml.Serialization;
using System.IO;
namespace GameDev.Data
{
    public class Member
    {
        /// <summary>
        /// 이름.
        /// </summary>
        public string Name;


        /// <summary>
        /// 스트레스 수치
        /// </summary>
        public int StressLevel;


        /// <summary>
        /// 노동력
        /// </summary>
        public int WorkForce;


        /// <summary>
        /// 의욕
        /// </summary>
        public int Will;

        // 이 위로 주요스텟

        // 이 밑으로 서브스텟


        /// <summary>
        /// 기획력
        /// </summary>
        public int DesignForce;

        /// <summary>
        /// 개발력
        /// </summary>
        public int DevelopmentForce;

        // 기타 등등?

        /// <summary>
        /// 인물에 대한 설명
        /// </summary>
        public string Description;

        /// <summary>
        /// 지금 동방에 나와 있는가? 출석
        /// </summary>
        public bool Attendence;

        /// <summary>
        /// 이 인간이 동방에 나올 확률. (0 ~ 100, 단위는 %)
        /// </summary>
        public int AttendenceProbability;
        
        /// <summary>
        /// 상태
        /// </summary>
        public string Status;

        /// <summary>
        /// 매일마다 회복하는 수치
        /// </summary>
        public int Heal;

        public int RemainingStunTurn;

        public string StunReason;

        public void GetStun(int duration, string reason)
        {

            StunReason = reason;
            RemainingStunTurn = duration;
        }

        public float GetWork()
        {
            return (WorkForce + Will) * (0.9f + ((Attendence) ? 0.1f : 0.0f)) * (200.0f - StressLevel) / 44000.0f; 
        }

        public Member(string name)
        {
            Name = name;
            StressLevel = 30 + RandomHelper.RandRange(-15, 16);
            WorkForce = 30 + RandomHelper.RandRange(-15,16);
            Will = 30 + RandomHelper.RandRange(-15, 16);
            Heal = 3 + RandomHelper.RandRange(-2, 3);
            AttendenceProbability = 80 + RandomHelper.RandRange(-10, 11);
        }

        public void Recalculate()
        {
            if (Will < 0)
                Will = 0;
            if (StressLevel < 0)
                StressLevel = 0;
            if (WorkForce < 0)
                WorkForce = 0;
            if (Will >= 100)
                Will = 100;
            if (StressLevel >= 100)
                StressLevel = 100;
            if (WorkForce >= 100)
                WorkForce = 100;
        }
        /*
        /// <summary>
        /// 상태이상 저항력
        /// </summary>
        public int Resistance;

        */

        // 앞으로 스텟 같은 걸 추가해야 함

        #region XML
        static public void Xmlize(Member eData, string path)
        {
            XmlSerializer ser = new XmlSerializer(typeof(EventData));
            using (TextWriter writer = new StreamWriter(path))
            {
                ser.Serialize(writer, eData);
            }
        }
        static public void Xmlize(Member eData)
        {
            Xmlize(eData, @"C:\Users\하제\Desktop\새 폴더\Test.xml");
        }
        public void Xmlize(string path)
        {
            Xmlize(this, path);
        }
        public void Xmlize()
        {
            Xmlize(this);
        }
        static public Member XmlLoad(string path)
        {
            Member eData;
            using (TextReader reader = new StreamReader(path))
            {
                XmlSerializer ser = new XmlSerializer(typeof(Member));
                eData = (Member)ser.Deserialize(reader);
            }
            return eData;
        }
        #endregion
    }
}
