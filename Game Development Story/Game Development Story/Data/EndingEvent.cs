﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameDev.Data
{
    class EndingEvent
    {
        public string Name;
        public List<string> Text = new List<string>();

        //public Predicate<Member> checkFunc = m => true;
        public Func<List<Member>, Project, bool> checkFunc;

        public void EventTrigger()
        {
            Program.NewsMessages = "\n";
            Program.Messages = new List<string>();
            Program.isEnding = true;
            Program.AddMessage(Name);
            System.Threading.Thread.Sleep(800);
            foreach (string str in Text)
            {
                Program.AddMessage(str);
                System.Threading.Thread.Sleep(800);
            }
            Program.DisplayEndingScreen();
        }
    }
}
