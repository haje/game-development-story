﻿using System;

namespace GameDev
{
    public static class RandomHelper
    {
        private static Random rd;

        static RandomHelper()
        {
            rd = new Random();
        }
        public static int RollDice()
        {
            return rd.Next(0, 100);
        }

        public static int RandRange(int minValue, int maxValue)
        {
            return rd.Next(minValue, maxValue);
        }

        public static bool DiceTest(int testValue)
        {
            return rd.Next(0, 100) > testValue;
        }
    }
}
